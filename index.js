var IList = function () {
};

IList.prototype.init = () => {
};
IList.prototype.getSize = () => {
};
IList.prototype.toString = () => {
};
IList.prototype.pop = () => {
};
IList.prototype.push = (value) => {
};
IList.prototype.shift = () => {
};
IList.prototype.unshift = (value) => {
};
IList.prototype.get = (index) => {
};
IList.prototype.set = (index, element) => {
};
IList.prototype.set = (index, element) => {
};
IList.prototype.sort = (comparator) => {
}; // comparator ==> callback

IList.prototype.slice = (start, end) => {
};
IList.prototype.splice = (start, numberToSplice, ...elements) => {
};

var Node = function (value) {
    this.value = value;
    this.next = null;
};

var LinkedList = function () {
    IList.apply(this, arguments);
    this.root = null;
};

LinkedList.prototype = Object.create(IList.prototype);
LinkedList.prototype.constructor = LinkedList;

LinkedList.prototype.init = function (initialArray) {
    if (initialArray.length === 0) {
        this.root = null;
    } else {
        for (var i = initialArray.length - 1; i >= 0; i--) {
            this.unshift(initialArray[i]);
        }
    }
};

LinkedList.prototype.getSize = function () {
    var tempNode = this.root;
    var size = 0;
    while (tempNode !== null) {
        tempNode = tempNode.next;
        size++;
    }
    return size;
};

// LinkedList.prototype.copy = function () {
//     var listResult = new LinkedList();
//     listResult.init([this.root.value]);
//     var tempNode = this.root.next;
//     var size = 0;
//     while (tempNode !== null) {
//         listResult.push(tempNode.value);
//         tempNode = tempNode.next;
//         size++;
//     }
//     return listResult;
// };

LinkedList.prototype.unshift = function (value) {
    var size = this.getSize();
    var node = new Node(value);
    node.next = this.root;
    this.root = node;
    return size + 1;
};

LinkedList.prototype.toStringg = function () {
    var tempNode = this.root;
    var str = '';
    while (tempNode !== null) {
        tempNode.next ? str += tempNode.value + ',' : str += tempNode.value + '';
        tempNode = tempNode.next
    }
    return str;
};

LinkedList.prototype.push = function (value) {
    var size = this.getSize();
    if (this.root === null) {
        var array = arguments;
        this.init(array);
        return ++size;
    } else {
        var tempNode = this.root;
        while (tempNode !== null) {
            if (tempNode.next === null) {
                tempNode.next = new Node(value);
                return ++size;
            }
            tempNode = tempNode.next;
        }
    }
};

LinkedList.prototype.pop = function () {
    var size = this.getSize();
    if (size === 0) {
        return undefined;
    } else {
        var tempNode = this.root;
        var result;
        while (size !== 2) {
            size--;
            tempNode = tempNode.next;
        }
        result = tempNode.next.value;
        tempNode.next = null;
        return result;
    }
};

LinkedList.prototype.shift = function () {
    var result = this.root.value;
    this.root = this.root.next;
    return result
};

LinkedList.prototype.get = function (index) {
    var tempNode = this.root;
    var size = 0;
    while (size !== index) {
        tempNode = tempNode.next;
        size++;
    }
    return tempNode;
};

LinkedList.prototype.set = function (index, element) {
    var tempNode = this.root;
    var size = 0;
    while (size !== index) {
        tempNode = tempNode.next;
        size++;
    }
    tempNode.value = element;
};

LinkedList.prototype.sort = function () {
    var temp;
    var tempNode = this.root;
    var flag = false;
    for (var i = 0; i < this.getSize(); i++) {
        while (tempNode.next !== null) {
            if (tempNode.value > tempNode.next.value) {
                temp = tempNode.next.value;
                tempNode.next.value = tempNode.value;
                tempNode.value = temp;
                flag = true;
            }
            // if (tempNode.next.value === null) {
            //     return;
            // }
            tempNode = tempNode.next;
        }
        tempNode = this.root;
            if (flag === false) {
                return;
            }

    }
};

LinkedList.prototype.deleteAt = function (index) {

    if (index === 0) {
        this.root = this.root.next;
        return;
    }
    const previous = this.getAt(index - 1);
    if (!previous || !previous.next) {
        return;
    }
    previous.next = previous.next.next;
    return this.root
};

LinkedList.prototype.getAt = function (index) {
    let counter = 0;
    let node = this.root;
    while (node) {
        if (counter === index) {
            return node;
        }
        counter++;
        node = node.next;
    }
};

LinkedList.prototype.slice = function (start, end) {
    //console.log(result);
    if (arguments.length < 2) {
        end = this.getSize();

    }
    var result = new LinkedList();
    result.init([]);
    var node = this.get(start);
    for (var i = start; i < end; i++) {
        //  console.log(node.value);
        result.push(node.value);
        if (node.next) {
            node = node.next;
        }
        //
    }

    return result;
};

LinkedList.prototype.splice = function (start, end) {
    var to;
    if (arguments.length === 1) {
        to = this.getSize();
        for (var i = start; i <= to; i++) {
            this.deleteAt(start);
        }
    }
    if (arguments.length === 2) {
        to = start + end;
        for (i = start; i < to; i++) {
            this.deleteAt(start);
        }
    }
    if (arguments.length > 2) {
        var argumentsList = new LinkedList();
        for (i = 2; i < arguments.length; i++) {
            argumentsList.push(arguments[i]);
        }
        var deleteTopIndex = start + end;
        var node = this.get(deleteTopIndex);
        for (i = deleteTopIndex; i < this.getSize(); i++) {
            argumentsList.push(node.value);
            node = node.next;
        }
        to = this.getSize();
        for (i = start; i <= to; i++) {
            this.deleteAt(start);
        }
        var argNode = argumentsList.get(0);
        for (i = 0; i < argumentsList.getSize(); i++) {
            this.push(argNode.value);
            argNode = argNode.next;
        }
    }
};

module.exports = LinkedList;