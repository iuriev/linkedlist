var assert = require("chai").assert;
var LL = require("./index");

describe('test LinkedList getSize function', function () {
    var testList;
    before("", () => {
        testList = new LL();
    });
    it('should return 2  when getSize of LinkedList initialized with [1,2]', function () {
        testList.init([1, 2]);
        assert.equal(testList.getSize(), 2);
    });
    it('should return 0  when getSize of LinkedList initialized with empty array', function () {
        testList.init([]);
        assert.equal(testList.getSize(), 0);
    });
    after("", () => {
        testList = null;
    })
});


describe('test LinkedList toString function', function () {
    var testList;
    before("", () => {
        testList = new LL();
    });
    it('should return string 1,2  when toString of LinkedList initialized with [1,2]', function () {
        testList.init([1, 2]);
        assert.equal(testList.toStringg(), "1,2");
    });
    it('should return empty string when toString of LinkedList initialized with empty array', function () {
        testList.init([]);
        assert.equal(testList.toStringg(), "");
    });
    after("", () => {
        testList = null;
    })
});

describe('test LinkedList pop function', function () {
    var testList;
    var resultList
    before("", () => {
        testList = new LL();
        resultList = new LL();
    });
    it('should return 3  when pop of LinkedList initialized with [1,2,3] and new LinkedList state should be [1,2]', function () {
        testList.init([1, 2, 3]);
        resultList.init([1,2]);
        var popValue = testList.pop();
        assert.deepEqual(testList, resultList);
        assert.equal(popValue, 3);
    });
    it('should return undefined  when pop of LinkedList initialized with empty array', function () {
        testList.init([]);
        var popValue = testList.pop();
        assert.equal(popValue, undefined);
    });
    after("", () => {
        testList = null;
    })
});

describe('test LinkedList deleteAt function', function () {
    var testList;
    var resultList;
    before("", () => {
        testList = new LL();
        resultList = new LL();
    });

    it('should return [1,2,3]  when push 3 to LinkedList initialized with 1,2]', function () {
        testList.init([1,2,3,4]);
        resultList.init([1,2,3]);
        testList.deleteAt(3);
        assert.deepEqual(testList, resultList);
    });

    it('should return [2,3,4]  when push 3 to LinkedList initialized with 1,2]', function () {
        testList.init([1,2,3,4]);
        resultList.init([2,3,4]);
        testList.deleteAt(0);
        assert.deepEqual(testList, resultList);
    });
    after("", () => {
        testList = null;
    })
});

describe('test LinkedList push function', function () {
    var testList;
    var resultList;
    before("", () => {
        testList = new LL();
        resultList = new LL();
    });
    it('should return [1,2,3]  when push 3 to LinkedList initialized with 1,2]', function () {
        testList.init([1,2]);
        resultList.init([1,2,3]);
        testList.push(3);
        assert.deepEqual(testList, resultList);
    });
    after("", () => {
        testList = null;
    })
});

describe('test LinkedList shift function', function () {
    var testList;
    var resultList;
    before("", () => {
        testList = new LL();
        resultList = new LL();
    });
    it('should return LinkedList contains [2]  when shift from LinkedList initialized with [1,2]', function () {
        testList.init([1,2]);
        resultList.init([2]);
        testList.shift();
        assert.deepEqual(testList, resultList);
    });
    after("", () => {
        testList = null;
    })
});

describe('test LinkedList unshift function', function () {
    var testList;
    var resultList;
    before("", () => {
        testList = new LL();
        resultList = new LL();
    });
    it('should return LinkedList contains [1,2,3]  when unshift to LinkedList initialized with [2,3]', function () {
        testList.init([2,3]);
        resultList.init([1,2,3]);
        testList.unshift(1);
        assert.deepEqual(testList, resultList);
    });
    after("", () => {
        testList = null;
    })
});

describe('test LinkedList get function', function () {
    var testList;
    var resultList;
    before("", () => {
        testList = new LL();
        resultList = new LL();
    });
       it('should return 4 when get element by third index of LinkedList initialized with [1,2,3,4]', function () {
        var Node = function(value) {
            this.value = value;
            this.next = null;
        };
        testList.init([1,2,3,4]);
        var newNode = new Node(4);
        var getResult = testList.get(3);
        assert.deepEqual(getResult, newNode);
    });
    after("", () => {
        testList = null;
    })
});

describe('test LinkedList set function', function () {
    var testList;
    var resultList;
    before("", () => {
        testList = new LL();
        resultList = new LL();
    });
    it('should return 1,9,3,4 when set  element on second index value = 9 in LinkedList initialized with [1,2,3,4]', function () {
        testList.init([1,2,3,4]);
        testList.set(1,9);
        var result = new LL();
        result.init([1,9,3,4]);
        assert.deepEqual(testList, result);
    });
    after("", () => {
        testList = null;
    })
});

describe('test LinkedList sort function', function () {
    var testList;
    var resultList;
    beforeEach("", () => {
        testList = new LL();
        resultList = new LL();
    });
    it('should return LinkedList 1,4,8,10 when sort LinkedList initialized with [1,4,8,10]', function () {
        testList.init([1,4,8,10]);
        testList.sort();
        resultList.init([1,4,8,10]);
        assert.deepEqual(testList, resultList);
    });
    it('should return LinkedList 10,8,4,1 when sort LinkedList initialized with [1,10,8,4]', function () {
        testList.init([1,10,8,4]);
        testList.sort();
        resultList.init([1,4,8,10]);
        assert.deepEqual(testList, resultList);
    });
    afterEach("", () => {
        testList = null;
        resultList = null;
    });
});

describe('test LinkedList slice function', function () {
    var testList;
    var resultList;
    var slicedPart;
    beforeEach("", () => {
        testList = new LL();
        resultList = new LL();
        slicedPart = new LL();
    });

    it('should return [3] when slice (2) of LinkedList initialized with [1,2,3]', function () {
        testList.init([1,2,3]);
        slicedPart = testList.slice(2);
        resultList.init([3]);
        assert.deepEqual(slicedPart, resultList);
    });
    it('should return [3] when slice (2,3) of LinkedList initialized with [1,2,3,4,5]', function () {
        testList.init([1,2,3,4,5]);
        slicedPart = testList.slice(2,3);
        resultList.init([3]);
        assert.deepEqual(slicedPart, resultList);
    });
    it('should return [1,2] when slice (0,2) of LinkedList initialized with [1,2,3,4,5]', function () {
        testList.init([1,2,3,4,5]);
        slicedPart = testList.slice(0,2);
        resultList.init([1,2]);
        assert.deepEqual(slicedPart, resultList);
    });
    it('should return [3,4] when slice (2) of LinkedList initialized with [1,2,3,4]', function () {
        testList.init([1,2,3,4,5]);
        slicedPart = testList.slice(2);
        resultList.init([3,4,5]);
        assert.deepEqual(slicedPart, resultList);
    });
    afterEach("", () => {
        testList = null;
        resultList = null;
        slicedPart = null;
    });

});

describe('test LinkedList splice', function () {
    var testList;
    var resultList;
    beforeEach("", () => {
        testList = new LL();
        resultList = new LL();
    });
    it('should make LL [1,2] when splice (2) of LinkedList initialized with [1,2,3,4,5]', function () {
        testList.init([1,2,3,4,5]);
        testList.splice(2);
        resultList.init([1,2]);
        assert.deepEqual(testList, resultList);
    });

    it('should make LL [1,2,5] when slice (2,2) of LinkedList initialized with [1,2,3,4,5]', function () {
        testList.init([1,2,3,4,5]);
        testList.splice(2,2);
        resultList.init([1,2,5]);
        assert.deepEqual(testList, resultList);
    });

    it('should make LL [1,2, 90, 6, 7, 8] when splice (2,,90) of LinkedList initialized with [1,2,3,4,5,6,7,8]', function () {
        testList.init([1,2,3,4,5,6,7 ,8]);
        testList.splice(2,3,90);
        resultList.init([1,2,90,6,7,8]);
        assert.deepEqual(testList, resultList);
    });

    it('should make LL [1,2,90,88,87,86, 6,7,8] when splice(2,3,90,88,87,86) of LinkedList initialized with [1,2,3,4,5,6,7 ,8]', function () {
        testList.init([1,2,3,4,5,6,7 ,8]);
        testList.splice(2,3,90,88,87,86);
        resultList.init([1,2,90,88,87,86, 6,7,8]);
        assert.deepEqual(testList, resultList);
    });
    afterEach("", () => {
        testList = null;
        resultList = null;
    })
});




